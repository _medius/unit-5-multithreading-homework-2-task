package com.epam.philosophers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	private static final int NUMBER_OF_THREADS = 5;

	private List<Fork> forks = new ArrayList<>();
	private List<Philosopher> philosophers = new ArrayList<>();

	private void createForks() {
		for (int i = 0; i < NUMBER_OF_THREADS; i++) {
			forks.add(new Fork());
		}
	}

	private void startPhilosophers() {
		ExecutorService service = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		for (int i = 0; i < NUMBER_OF_THREADS; i++) {
			service.submit(new Philosopher(forks));
		}
		service.shutdown();
	}

	private void appStart() {
		createForks();
		startPhilosophers();
	}

	public static void main(String[] args) {
		new Main().appStart();
	}
}
