package com.epam.philosophers;

public class Fork {
	private static int idSequence = 0;

	{
		id = ++idSequence;
	}

	private final int id;

	public static int getIdSequence() {
		return idSequence;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "fork " + id + '.';
	}
}
