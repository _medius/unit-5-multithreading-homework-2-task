package com.epam.philosophers;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Philosopher implements Runnable {

	private static int idSequence = 0;

	{
		id = ++idSequence;
	}

	private final int id;

	private List<Fork> forks;
	private Fork forkWithGreaterNumber;
	private Fork forkWithLesserNumber;

	public Philosopher(List<Fork> forks) {
		this.forks = forks;
	}

	private void chooseForks() {
		Fork firstFork = forks.get(this.id - 1);
		Fork secondFork;

		if (this.id == 1) {
			secondFork = forks.get(forks.size() - 1);
		} else {
			secondFork = forks.get(this.id - 2);
		}

		if (firstFork.getId() > secondFork.getId()) {
			forkWithGreaterNumber = firstFork;
			forkWithLesserNumber = secondFork;
		} else {
			forkWithGreaterNumber = secondFork;
			forkWithLesserNumber = firstFork;
		}
	}

	@Override
	public void run() {

		chooseForks();

		while (true) {
			try {
			System.out.println(this + " thinking.");
			TimeUnit.MILLISECONDS.sleep(500);

			synchronized (forkWithLesserNumber) {
					System.out.println(this + " get " + forkWithLesserNumber);

					synchronized (forkWithGreaterNumber) {
						System.out.println(this + " get " + forkWithGreaterNumber);
						System.out.println(this + " eating");
						TimeUnit.MILLISECONDS.sleep(500);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		return "Philosopher " + this.id;
	}
}
